@if ($paginator->lastPage() > 1)
    <div class="flex-wr-s-c m-rl--7 p-t-15">
        {{-- <a href="{{ $paginator->url(1) }}" class="flex-c-c pagi-item hov-btn1 trans-03 m-all-7 {{ ($paginator->currentPage() == 1) ? 'disabled' : '' }}">Sebelum</a> --}}
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <a href="{{ $paginator->url($i) }}" class="flex-c-c pagi-item hov-btn1 trans-03 m-all-7 {{ ($paginator->currentPage() == $i) ? 'page-active' : '' }}">{{ $i }}</a>
        @endfor
        {{-- <a href="{{ $paginator->url($paginator->currentPage()+1) }}" class="flex-c-c pagi-item hov-btn1 trans-03 m-all-7 {{ ($paginator->currentPage() == $paginator->lastItem()) ? 'page-active' : '' }}">Selanjutnya</a> --}}
    </div>
</nav>
@endif