@extends('index')

@section('content')
<h1 class="mt-4">Dashboard</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Dashboard</li>
</ol>
    <div class="row">
        @if($role == 1)
        <div class="col-md-4">
            <div class="p-3 mb-3 text-white" style="background-color:#653208"> 
                Total Guru : {{ $guru }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="p-3 mb-3 bg-danger text-white"> 
                Total guru Ditolak : {{ $guruditolak }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="p-3 mb-3 bg-success text-white"> 
                Total Guru Diterima : {{ $guruditerima }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="p-3 mb-3 bg-primary text-white"> 
                Total Bank Soal : {{ $banksoal }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="p-3 mb-3 text-white" style="background-color:#20c997"> 
                Total Materi : {{ $materidigital }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="p-3 mb-3 text-white" style="background-color:#0dcaf0"> 
                Total Sekolah : {{ $sekolah }}
            </div>
        </div>
    </div>
    @else 
        <div class="col-md-4">
            <div class="p-3 mb-3 bg-primary text-white"> 
                Total Bank Soal {{ $username }}: {{ $banksoalself }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="p-3 mb-3 text-white" style="background-color:#3d0a91"> 
                Total Bank Soal keseluruhan: {{ $banksoal }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="p-3 mb-3 text-white" style="background-color:#20c997"> 
                Total Materi {{ $username }}: {{ $materidigitalself }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="p-3 mb-3 text-white" style="background-color:#052c65"> 
                Total Materi keseluruhan: {{ $materidigital }}
            </div>
        </div>
    @endif
@endsection