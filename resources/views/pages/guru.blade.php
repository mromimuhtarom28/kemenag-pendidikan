@extends('index')

@section('content')
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Persetujuan akun guru
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Foto Profil</th>
                        <th>NUPTK</th>
                        <th>No. KTP</th>
                        <th>Nama Lengkap</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Username</th>
                        <th>Alamat</th>
                        <th>Tanggal Lahir</th>
                        <th>No. Telp</th>
                        <th>Kata Sandi</th>
                        <th>Status</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($guru as $gk)
                    <tr>
                        <td align="center">
                            <div  style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;">
                                <img src="http://192.168.1.8:8000/images/profile/{{ $gk->id_login }}.jpg" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;magin-top:auto;margin-bottom:auto;">
                            </div>
                        </td>
                        <td>{{ $gk->nuptk }}</td>
                        <td>{{ $gk->no_ktp }}</td>
                        <td>{{ $gk->nama_lengkap }}</td>
                        <td>{{ $gk->email }}</td>
                        <td>
                            @if ($gk->jk == 1)
                                Laki - Laki
                            @elseif($gk->jk == 2)
                                Perempuan
                            @endif
                        </td>
                        <td>{{ $gk->username }}</td>
                        <td>{{ $gk->alamat }}</td>
                        <td>{{ $gk->ttl }}</td>
                        <td>{{ $gk->telp }}</td>
                        <td><a href="#" data-pk="{{ $gk->id_login }}" data-toggle="modal" data-target="#updatepwd" class="btn btn-primary changepwd">Ganti Kata Sandi</a></td>
                        <td>
                            @if ($gk->status == 0)
                                <span style="color:red;font-weight:bold;">Ditolak</span><br>
                                <span>
                                    Catatan: <br>
                                    {{ $gk->catatan }}
                                </span>
                            @elseif($gk->status == 2)  
                                <span style="color:green;font-weight:bold;">Diterima</span>                              
                            @endif
                        </td>
                        <td><a href="#" data-pk="{{ $gk->id_login }}" data-toggle="modal" data-target="#deleteakun" class="deleteaccount"><i style="color:red" class="fas fa-times"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $guru->links('pagination.default') }}</div>
        </div>
    </div>
</div> 

{{-- Delete Data --}}
<div class="modal fade" id="deleteakun" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menambahkan Akun Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Guru-destroy') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    Apakah anda yakin ingin menghapus akun ini ? <br>
                    <input type="hidden" name="pk" id="pk-destroy">
                    <input type="password" name="kataSandiSedangLogin" placeholder="Kata sandi akun anda" class="form-control" required>
                </div> 
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Hapus Akun</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- Update password --}}
<div class="modal fade" id="updatepwd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Mengubah Kata sandi admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Guru-chgpwd') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    <div class="breadcrumb" align="left">
                        Catatan : <br>
                        - untuk kata sandi yang sedang login adalah dari akun yang lagi login sekarang
                    </div>
                    <input type="hidden" name="pk" id="pk-pwd">
                    <input type="password" name="kataSandiBaru" placeholder="Kata Sandi Baru" class="form-control" required><br>
                    <input type="password" name="kataSandiSedangLogin" placeholder="Kata sandi akun anda" class="form-control" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ubah Kata Sandi</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('.btn-approve').click(function(){
        var idnikah = $(this).attr('data-idguru');
        $('#approve-pk').val(idnikah);
    })

    $('.btn-decline').click(function(){
        var idnikah = $(this).attr('data-idguru');
        $('#decline-pk').val(idnikah);
    })

    $('.deleteaccount').click(function(){
        var pk = $(this).attr('data-pk');
        $('#pk-destroy').val(pk);
    })

    $('.changepwd').click(function(){
        var pk = $(this).attr('data-pk');
        $('#pk-pwd').val(pk);
    });

    $(document).ready(function() {
        $('table.table').DataTable({
            "bLengthChange": false,
            "searching": false,
            "paging":false,
            "bInfo":false,
            "ordering": false,
        });
    }); 
</script>
@endsection