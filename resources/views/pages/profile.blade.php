@extends('index')

@section('content')
<script>
    function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imginsert').attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
</script>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active border border-dark" style="width:100%;">
        <form action="{{ route('Profile-Update') }}" method="post" enctype="multipart/form-data" style="width:100%">
            @csrf
            <table align="center">
                <tr>
                    <td Colspan="3" style="color:black;font-weight:bold;" align="center" height="105px">Ubah Profile</td>
                </tr>
                @if($guru->catatan != NULL && $guru->status == 0)
                <tr>
                    <td colspan="3" align="center">
                        <div class="bg-danger text-light" style="border-radius:5px;padding:5px;" align="left">
                            Catatan : <br>
                            {{ $guru->catatan }}
                        </div>
                    </td>
                </tr>
                @endif
                <input type="hidden" name="pk" value="{{ $guru->id_login }}">

                <tr>
                    @php 
                    if(does_url_exists('http://192.168.1.14:3000/images/profile/'.$guru->id_login.'.jpg')): 
                        $filelocation = 'http://192.168.1.14:3000/images/profile/'.$guru->id_login.'.jpg';
                    else: 
                        $filelocation = '/admin/assets/img/uploadimg.png';
                    endif;
                    @endphp
                    <td align="center" colspan="3">
                        <div style="border-radius:10px;border:1px solid black;background-color:#cccccc;width:200px;height:100px;position: relative;display: inline-block;">
                            <img src="{{ $filelocation }}" alt="" id="imginsert" max-width="100px" height="98px">
                        </div><br>                                    
                        <input type="file" accept="image/jpg" name="gambarProfile" id="btnimginsert" onchange="readURL(this);">
                    </td>
                </tr>
                <tr>
                    <td width="47%" style="color:black;font-weight:bold;">No. KTP</td>
                    <td width="6%" align="center">:</td>
                    <td width="47%"><input type="number" name="noKtp" class="form-control" value="{{ $guru->no_ktp }}" required></td>
                </tr>
                <tr>
                    <td width="47%" style="color:black;font-weight:bold;">NUPTK</td>
                    <td width="6%" align="center">:</td>
                    <td width="47%"><input type="number" name="nuptk" class="form-control" value="{{ $guru->nuptk }}" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Nama Lengkap</td>
                    <td align="center">:</td>
                    <td><input type="text" name="namaLengkap" class="form-control" value="{{ $guru->nama_lengkap }}" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Nama Pengguna</td>
                    <td align="center">:</td>
                    <td>{{ $guru->username }}</td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Sekolah</td>
                    <td align="center">:</td>
                    <td>
                        <select name="sekolah" id="" class="form-control" required>
                            <option value="">Pilih Sekolah</option>
                            @foreach ($sekolah as $sk)
                                <option value="{{ $sk->id_sekolah}}" @if($guru->id_sekolah == $sk->id_sekolah) selected @endif>{{ $sk->nama_sekolah }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Email</td>
                    <td align="center">:</td>
                    <td>{{ $guru->email }}</td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">No Telp</td>
                    <td align="center">:</td>
                    <td><input type="text" name="noTelp" class="form-control" value="{{ $guru->telp }}" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Tanggal Lahir</td>
                    <td align="center">:</td>
                    <td><input type="date" name="tanggalLahir" class="form-control" value="{{ $guru->ttl }}" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Alamat</td>
                    <td align="center">:</td>
                    <td><textarea name="alamat" class="form-control" cols="30" rows="5" required>{{ $guru->alamat }}</textarea></td>
                </tr>
                <tr>
                    <td colspan="3" align="center" height="105px"><button type="submit" class="btn btn-primary">Ubah Data</button></td>
                </tr>
            </table>
        </form>
    </li>
</ol>

@endsection