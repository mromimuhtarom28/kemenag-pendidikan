@extends('index')

@section('content')
<script>
    function readURLeditimg(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imgaktenikahdisdukcapil').attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
</script>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active" style="width:100%;">
        <form action="{{ route('BankSoal') }}">
            <table align="center" >
                <tr>
                    <td>
                        <input type="text" name="namaInput" class="form-control" value="{{ $namainput }}" placeholder="Nama Pengguna Pengupload">
                    </td>
                    <td>
                        <select name="tingkatan" class="form-control tingkatan">
                            <option value="">Tingakatan</option>
                            <option value="sd" @if($tingkatan == 'sd') selected @endif>SD</option>
                            <option value="smp" @if($tingkatan == 'smp') selected @endif>SMP</option>
                            <option value="sma/smk" @if($tingkatan == 'sma/smk') selected @endif>SMA / SMK</option>
                        </select>
                    </td>
                    <td>
                        <select name="kelas" class="form-control kelas">
                            <option value="">Kelas</option>
                            @if($tingkatan == 'sd')
                            <option value="1" @if($kelas == 1) selected @endif class="kelasopt">1</option>
                            <option value="2" @if($kelas == 2) selected @endif class="kelasopt">2</option>
                            <option value="3" @if($kelas == 3) selected @endif class="kelasopt">3</option>
                            <option value="4" @if($kelas == 4) selected @endif class="kelasopt">4</option>
                            <option value="5" @if($kelas == 5) selected @endif class="kelasopt">5</option>
                            <option value="6" @if($kelas == 6) selected @endif class="kelasopt">6</option>
                            @elseif($tingkatan == 'smp')
                            <option value="4" @if($kelas == 7) selected @endif class="kelasopt">7</option>
                            <option value="5" @if($kelas == 8) selected @endif class="kelasopt">8</option>
                            <option value="6" @if($kelas == 9) selected @endif class="kelasopt">9</option>
                            @elseif($tingkatan == 'sma/smk')
                            <option value="4" @if($kelas == 10) selected @endif class="kelasopt">10</option>
                            <option value="5" @if($kelas == 11) selected @endif class="kelasopt">11</option>
                            <option value="6" @if($kelas == 12) selected @endif class="kelasopt">12</option>
                            @endif
                        </select>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-primary">
                            Cari    
                        </button>
                    </td>
                </tr>
            </table>
        </form>
    </li>
</ol>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Data Bank Soal
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div>
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#adddata">Input Bank Soal</a>
            </div>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>File Materi Digital</th>
                        <th>kelas & tingkatan</th>
                        <th>Semester</th>
                        <th>tgl upload</th>
                        <th>Nama Pengguna Pengupload</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($materiDigital as $md)
                    <tr>
                        <td><a href="/upload/materi_digital/{{ $md->nama_file }}" target="_blank">Link Materi Digital</a></td>
                        <td>{{ $md->kelas }} {{ $md->tingkatan }}</td>
                        <td>
                            @if ($md->semester == 1)
                                Ganjil
                            @else     
                                Genap                           
                            @endif
                        </td>
                        <td>{{ $md->tgl_upload }}</td>
                        <td>{{ $md->nama_lengkap }}</td>
                        <td>
                            @if($md->id_login == $idlogin)
                            <a href="#" class="btn btn-primary btn-edit"  data-toggle="modal" data-target="#editdata" data-pk="{{ $md->id_materi_digital }}" data-tingkatan="{{ $md->tingkatan }}" data-kelas="{{ $md->kelas }}" data-semester="{{ $md->semester }}" data-catatan="{{ $md->catatan }}">Edit Data</a>
                            @endif
                        </td>
                        <td>
                            <a href="#" class="btn-delete" data-pk="{{ $md->id_materi_digital }}"  data-toggle="modal" data-target="#deletedata">
                                <i class="fas fa-times" style="color:red"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $materiDigital->links('pagination.default') }}</div>
        </div>
    </div>
</div> 



<div class="modal fade" id="editdata" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data Bank Soal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('MateriDigital-update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="pk" id="pk-edit">
                    <span>Upload File Soal</span><br>
                    <input type="file" name="uploadMateri" style="content:'Input data'"><br><br>
                    <span>Tingkatan</span><br>
                    <select name="tingkatan" class="form-control tingkatan" id="tingkatan-edit" required>
                        <option value="">Tingkatan</option>
                        <option value="sd">SD</option>
                        <option value="smp">SMP</option>
                        <option value="sma/smk">SMA / SMK</option>
                    </select><br>
                    <span>Kelas</span><br>
                    <select name="kelas" class="form-control kelas" id="kelas-edit" required>
                        <option value="">Kelas</option>
                    </select><br>
                    <span>Semester</span><br>
                    <select name="semester" class="form-control" id="semester-edit" required>
                        <option value="">Semester</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Edit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>




<div class="modal fade" id="adddata" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambahkan Data Bank Soal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('MateriDigital-Input') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <span>Upload File Soal</span><br>
                    <input type="file" name="uploadMateri" style="content:'Input data'" required><br><br>
                    <span>Tingkatan</span><br>
                    <select name="tingkatan" class="form-control tingkatan" id="" required>
                        <option value="">Tingkatan</option>
                        <option value="sd">SD</option>
                        <option value="smp">SMP</option>
                        <option value="sma/smk">SMA / SMK</option>
                    </select><br>
                    <span>Kelas</span><br>
                    <select name="kelas" class="form-control kelas" id="" required>
                        <option value="">Kelas</option>
                    </select><br>
                    <span>Semester</span><br>
                    <select name="semester" class="form-control" id="" required>
                        <option value="">Semester</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Input</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal fade" id="deletedata" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('BankSoal-Delete') }}" method="POST">
                @csrf
                <div class="modal-body" align="center">
                    Apakah anda yakin ingn menghapus data ini ?   
                    <input type="hidden" name="pk" id="idbsoaldelete">                  
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Hapus Data</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="catatan-detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Catatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span>Kelas</span><br>
                <input type="text" class="form-control" name="" id="kelas-detail" disabled><br>
                <span>Semester</span><br>
                <input type="text" class="form-control" name="" id="semester-detail" disabled><br>
                <span>Catatan</span><br>
                <textarea name="" id="catatan-detail-txt" class="form-control" cols="30" rows="10" disabled></textarea>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>




<script>
    $('.tingkatan').change(function(){
        var valtingkatan = $(this).val();
        $('.kelasopt').remove();
        if(valtingkatan == 'sd') {
            $('.kelas').append('<option value="1" class="kelasopt">1</option><option value="2" class="kelasopt">2</option><option value="3" class="kelasopt">3</option><option value="4" class="kelasopt">4</option><option value="5" class="kelasopt">5</option><option value="6" class="kelasopt">6</option>');
        } else if(valtingkatan == 'smp') {
            $('.kelas').append('<option value="7" class="kelasopt">7</option><option value="8" class="kelasopt">8</option><option value="9" class="kelasopt">9</option>');
        } else if(valtingkatan == 'sma/smk') {
            $('.kelas').append('<option value="10" class="kelasopt">10</option><option value="11" class="kelasopt">11</option><option value="12" class="kelasopt">12</option>');
        } else {
            $('.kelas').append('<option class="kelasopt">Tingkatan Harus Diiisi</option>')
        }

    });

    $('.btn-delete').click(function(){
        var pk = $(this).attr('data-pk');
        $('#idbsoaldelete').val(pk);
    });

    $('.cttn-detail').click(function() {
        var tingkatan = $(this).attr('data-tingkatan');
        var kelas     = $(this).attr('data-kelas');
        var semester  = $(this).attr('data-semester');
        var catatan   = $(this).attr('data-catatan');
        $('#kelas-detail').val(kelas+' '+tingkatan);
        $('#semester-detail').val(semester);
        $('#catatan-detail-txt').val(catatan);
    })
    $('.btn-edit').click(function(){
        var idbsoal   = $(this).attr('data-pk');
        var tingkatan = $(this).attr('data-tingkatan');
        var kelas     = $(this).attr('data-kelas');
        var semester  = $(this).attr('data-semester');
        var catatan   = $(this).attr('data-catatan');
        $('#pk-edit').val(idbsoal);
        $('#tingkatan-edit').val(tingkatan);
        $('#semester-edit').val(semester);
        $('#catatan-edit').val(catatan);
        $('.kelasopt').remove();
        if(tingkatan == 'sd'){
            if(kelas == 1) {
                $('.kelas').append('<option value="1" selected class="kelasopt">1</option><option value="2" class="kelasopt">2</option><option value="3" class="kelasopt">3</option><option value="4" class="kelasopt">4</option><option value="5" class="kelasopt">5</option><option value="6" class="kelasopt">6</option>');
            } else if(kelas == 2) {
                $('.kelas').append('<option value="1" class="kelasopt">1</option><option selected value="2" class="kelasopt">2</option><option value="3" class="kelasopt">3</option><option value="4" class="kelasopt">4</option><option value="5" class="kelasopt">5</option><option value="6" class="kelasopt">6</option>');
            } else if(kelas == 3) {
                $('.kelas').append('<option value="1" class="kelasopt">1</option><option value="2" class="kelasopt">2</option><option value="3" selected class="kelasopt">3</option><option value="4" class="kelasopt">4</option><option value="5" class="kelasopt">5</option><option value="6" class="kelasopt">6</option>');
            } else if(kelas == 4) {
                $('.kelas').append('<option value="1" class="kelasopt">1</option><option value="2" class="kelasopt">2</option><option value="3" class="kelasopt">3</option><option value="4" selected class="kelasopt">4</option><option value="5" class="kelasopt">5</option><option value="6" class="kelasopt">6</option>');
            } else if(kelas == 5) {
                $('.kelas').append('<option value="1" class="kelasopt">1</option><option value="2" class="kelasopt">2</option><option value="3" class="kelasopt">3</option><option value="4" class="kelasopt">4</option><option value="5" selected class="kelasopt">5</option><option value="6" class="kelasopt">6</option>');
            } else if(kelas == 6) {
                $('.kelas').append('<option value="1" class="kelasopt">1</option><option value="2" class="kelasopt">2</option><option value="3" class="kelasopt">3</option><option value="4" class="kelasopt">4</option><option value="5" class="kelasopt">5</option><option value="6" selected class="kelasopt">6</option>');
            } else {
                $('.kelas').append('<option class="kelasopt">Tingkatan Harus Diiisi</option>')
            }

        } else if(tingkatan == 'smp') {
            if(kelas == 7) {
                $('.kelas').append('<option value="7" selected class="kelasopt">7</option><option value="8" class="kelasopt">8</option><option value="9" class="kelasopt">9</option>');
            } else if(kelas == 8) {
            
                $('.kelas').append('<option value="7" class="kelasopt">7</option><option value="8" selected class="kelasopt">8</option><option value="9" class="kelasopt">9</option>');

            } else if(kelas == 9) {

                $('.kelas').append('<option value="7" class="kelasopt">7</option><option value="8" class="kelasopt">8</option><option value="9" selected class="kelasopt">9</option>');

            } else {
                $('.kelas').append('<option class="kelasopt">Tingkatan Harus Diiisi</option>')
            }

        } else if(tingkatan == 'sma/smk') {

            if(kelas == 10) {
                $('.kelas').append('<option value="10" selected class="kelasopt">10</option><option value="11" class="kelasopt">11</option><option value="12" class="kelasopt">12</option>');
            } else if(kelas == 11) {
                $('.kelas').append('<option value="10" class="kelasopt">10</option><option value="11" class="kelasopt">11</option><option value="12" class="kelasopt">12</option>');
            } else if(kelas == 12) {
                $('.kelas').append('<option value="10" class="kelasopt">10</option><option value="11" selected class="kelasopt">11</option><option selected value="12" class="kelasopt">12</option>');
            } else {
                $('.kelas').append('<option class="kelasopt">Tingkatan Harus Diiisi</option>')
            }

        } else {
            $('.kelas').append('<option class="kelasopt">Tingkatan Harus Diiisi</option>')
        }


    });
</script>
@endsection