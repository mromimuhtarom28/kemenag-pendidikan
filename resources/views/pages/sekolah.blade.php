@extends('index')

@section('content')
<script>
    function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imginsert').attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }

    function readURLeditimg(input, idsekolah) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imgedit'+idsekolah).attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
</script>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Data Sekolah
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div>
                <table width="100%">
                    <tr>
                        <td align="left"><a href="#" data-toggle="modal" data-target="#createadmin" class="btn btn-primary">Menambahkan Sekolah</a></td>
                        <td align="right">
                            <form action="{{ route('Sekolah') }}">
                            <table>
                                <tr>
                                    <td><input type="text" name="namaSekolah" class="form-control" placeholder="Nama Sekolah" @if(isset($namasekolah)) value="{{ $namasekolah }}" @endif></td>
                                    <td><button type="submit" class="btn btn-primary">Cari</button></td>
                                </tr>
                            </table>
                            </form>
                            
                        </td>
                    </tr>
                </table>
            </div>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Gambar Sekolah</th>
                        <th>No. Ijin Sekolah</th>
                        <th>Nama Sekolah</th>
                        <th>Kode Pos</th>
                        <th>Kecamatan</th>
                        <th>Alamat</th>
                        <th>status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sekolah as $sk)
                        <tr>
                            <td align="center" class="kolomeditgambar{{ $sk->id_sekolah }}">
                                <div class="gambarbelumedit{{ $sk->id_sekolah }}"  style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;">
                                    <img class="gambarbelumedit{{ $sk->id_sekolah }}" src="/upload/sekolah/{{ $sk->id_sekolah }}.jpg?{{ Carbon\Carbon::now('GMT+7')}}" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;magin-top:auto;margin-bottom:auto;">
                                </div>
                                <br class="gambarbelumedit{{ $sk->id_sekolah}}">
                                <a href="#" class="btn btn-primary ubahgambar{{ $sk->id_sekolah}}">Ubah Gambar</a> 
                            </td>
                            <td><a href="#" class="textnormal" data-name="no_ijin_sekolah" data-pk="{{ $sk->id_sekolah }}" data-type="text" data-value="{{ $sk->no_ijin_sekolah }}" data-url="{{ route('Sekolah-update') }}">{{ $sk->no_ijin_sekolah }}</a></td>
                            <td><a href="#" class="textnormal" data-name="nama_sekolah" data-pk="{{ $sk->id_sekolah }}" data-type="text" data-value="{{ $sk->nama_sekolah }}" data-url="{{ route('Sekolah-update') }}">{{ $sk->nama_sekolah }}</a></td>
                            <td><a href="#" class="textnormal" data-name="kode_pos" data-pk="{{ $sk->id_sekolah }}" data-type="number" data-value="{{ $sk->kode_pos }}" data-url="{{ route('Sekolah-update') }}">{{ $sk->kode_pos }}</a></td>
                            <td><a href="#" class="textnormal" data-name="kecamatan" data-pk="{{ $sk->id_sekolah }}" data-type="text" data-value="{{ $sk->kecamatan }}" data-url="{{ route('Sekolah-update') }}">{{ $sk->kecamatan }}</a></td>
                            <td><a href="#" class="textnormal" data-name="alamat" data-pk="{{ $sk->id_sekolah }}" data-type="text" data-value="{{ $sk->alamat }}" data-url="{{ route('Sekolah-update') }}">{{ $sk->alamat }}</a></td>
                            <td><a href="#" class="statustext" data-name="status" data-pk="{{ $sk->id_sekolah }}" data-type="select" data-value="{{ $sk->status }}" data-url="{{ route('Sekolah-update') }}">{{ str_endis($sk->status) }}</a></td>
                            {{-- <td><i class="fas fa-times" data-toggle="modal" data-target="#deletevihara" style="color:red"></i></td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $sekolah->links('pagination.default') }}</div>
        </div>
    </div>
</div> 


{{-- insert data --}}
<div class="modal fade" id="createadmin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menambahkan Sekolah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Sekolah-processInsert') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <div style="border-radius:10px;border:1px solid black;background-color:#cccccc;width:200px;height:100px;position: relative;display: inline-block;">
                                    <img src="/admin/assets/img/uploadimg.png" alt="" id="imginsert" max-width="100px" height="98px">
                                </div><br>                                    
                                <input type="file" accept="image/jpg" name="file" id="btnimginsert" onchange="readURL(this);" required>
                            </td>
                        </tr>
                    </table><br>
                    <input type="text" name="noIjinSekolah" class="form-control" placeholder="No Ijin Sekolah"><br>
                    <input type="text" name="namaSekolah" placeholder="Nama Sekolah" class="form-control" required><br> 
                    <input type="number" name="kodePos" class="form-control" placeholder="Kode Pos"><br>
                    <input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan"><br>
                    <textarea name="alamat" class="form-control" cols="30" rows="10" placeholder="Alamat" required></textarea><br>
                    <select name="status" class="form-control" required>
                        <option value="">Pilih status</option>
                        <option value="0">Tidak Aktif</option>
                        <option value="1">Aktif</option>
                    </select>                  
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>

    $('.deleteaccount').click(function(){
        var pk = $(this).attr('data-pk');
        $('#pk-destroy').val(pk);
    });

    @foreach($sekolah as $sk)
        $(".ubahgambar{{ $sk->id_sekolah }}").on("click", function() {
            $(this).hide();
            $(".gambarbelumedit{{ $sk->id_sekolah }}").css("display", "none");
            $(".kolomeditgambar{{ $sk->id_sekolah }}").append('<form method="POST" action="{{ route("Sekolah-ImgChange") }}" enctype="multipart/form-data" class="editgambar{{ $sk->id_sekolah }}"> @csrf <div style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;"><img src="/upload/sekolah/{{ $sk->id_sekolah }}.jpg?{{ Carbon\Carbon::now("GMT+7")}}" id="imgedit{{ $sk->id_sekolah }}" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;"></div><br><input type="hidden" name="pk" value="{{ $sk->id_sekolah }}"><input type="file" onchange="readURLeditimg(this,"{{ $sk->id_sekolah }}")" class="btn btn-secondary" accept="image/jpeg" name="file-sekolah" id="editgambar" required><br><button type="submit" class="btn btn-success">Simpan</button> <a href="#" class="btn btn-danger editgambar" id="bataleditgambar{{ $sk->id_sekolah}}">Batal</a></form>');
        });

        $('.kolomeditgambar{{ $sk->id_sekolah}}').on('click', '#bataleditgambar{{ $sk->id_sekolah }}', function(){
            $(".ubahgambar{{ $sk->id_sekolah}}").show();
            $(".gambarbelumedit{{ $sk->id_sekolah}}").css("display", "block");
            $(".editgambar{{ $sk->id_sekolah}}").remove();
        });
    @endforeach
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('table.table').DataTable({
            "bLengthChange": false,
            "searching": false,
            "paging":false,
            "bInfo":false,
            "ordering": false,
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('.textnormal').editable({
                        mode :'inline',
                        validate: function(value) {
                            if($.trim(value) == '') {
                            return 'Tidak boleh kosong';
                            }
                        }
                    });


                    $('.datetest').editable({
                        format: 'yyyy-mm-dd',  
                        mode : 'popup',
                        viewformat: 'dd/mm/yyyy',    
                        datepicker: {
                                weekStart: 1
                        }
                    });



                    $('.statustext').editable({
                        mode:'inline',
                        source: [
                            {value: '', text: 'Pilih Kelas'},
                            {value: 0, text: 'Tidak Aktif'},
                            {value: 1, text: 'Aktif'},
                        ],
                        validate: function(value) {
                        if($.trim(value) == '') {
                            return 'Tidak boleh kosong';
                        }
                        }
                    });
            }
        });
    }); 
</script>
@endsection