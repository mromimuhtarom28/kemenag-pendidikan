@extends('index')

@section('content')
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Persetujuan akun guru
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Foto Profil</th>
                        <th>NUPTK</th>
                        <th>No. KTP</th>
                        <th>Nama Lengkap</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Username</th>
                        <th>Alamat</th>
                        <th>Tanggal Lahir</th>
                        <th>No. Telp</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($guru as $gk)
                    <tr>
                        <td align="center">
                            <div  style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;">
                                <img src="http://192.168.1.8:8000/images/profile/{{ $gk->id_login }}.jpg" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;magin-top:auto;margin-bottom:auto;">
                            </div>
                        </td>
                        <td>{{ $gk->nuptk }}</td>
                        <td>{{ $gk->no_ktp }}</td>
                        <td>{{ $gk->nama_lengkap }}</td>
                        <td>{{ $gk->email }}</td>
                        <td>
                            @if ($gk->jk == 1)
                                Laki - Laki
                            @elseif($gk->jk == 2)
                                Perempuan
                            @endif
                        </td>
                        <td>{{ $gk->username }}</td>
                        <td>{{ $gk->alamat }}</td>
                        <td>{{ $gk->ttl }}</td>
                        <td>{{ $gk->telp }}</td>
                        <td><a href="#" class="btn btn-success btn-approve" data-toggle="modal" data-idguru="{{ $gk->id_guru }}" data-target="#approve">Terima</a></td>
                        <td><a href="#" class="btn btn-danger btn-decline" data-toggle="modal" data-target="#decline" data-idguru="{{ $gk->id_guru }}">Ditolak</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $guru->links('pagination.default') }}</div>
        </div>
    </div>
</div> 

<div class="modal fade" id="approve" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menerima Data Ini</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('PenerimaanGuru-appdec') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="approve-pk" name="pk">
                    <input type="hidden" name="status" value="2">
                    Data telah diterima
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Diterima</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="decline" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menolak Data Ini</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('PenerimaanGuru-appdec') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="decline-pk" name="pk">
                    <input type="hidden" name="status" value="0">
                    Menolak data ini <br>
                    <textarea name="catatan" id="" cols="30" rows="10" class="form-control" placeholder="Catatan" required></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ditolak</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('.btn-approve').click(function(){
        var idnikah = $(this).attr('data-idguru');
        $('#approve-pk').val(idnikah);
    })

    $('.btn-decline').click(function(){
        var idnikah = $(this).attr('data-idguru');
        $('#decline-pk').val(idnikah);
    })

    $(document).ready(function() {
        $('table.table').DataTable({
            "bLengthChange": false,
            "searching": false,
            "paging":false,
            "bInfo":false,
            "ordering": false,
        });
    }); 
</script>
@endsection