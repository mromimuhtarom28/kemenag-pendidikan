@extends('index')

@section('content')
<script>
    function readURLeditimg(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imgaktenikahdisdukcapil').attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
</script>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active" style="width:100%;">
        <form action="{{ route('BankSoal-Semua') }}">
            <table align="center" >
                <tr>
                    <td>
                        <input type="text" name="namaInput" class="form-control" value="{{ $namainput }}" placeholder="Nama Pengguna Pengupload">
                    </td>
                    <td>
                        <select name="tingkatan" class="form-control tingkatan">
                            <option value="">Tingakatan</option>
                            <option value="sd" @if($tingkatan == 'sd') selected @endif>SD</option>
                            <option value="smp" @if($tingkatan == 'smp') selected @endif>SMP</option>
                            <option value="sma/smk" @if($tingkatan == 'sma/smk') selected @endif>SMA / SMK</option>
                        </select>
                    </td>
                    <td>
                        <select name="kelas" class="form-control kelas">
                            <option value="">Kelas</option>
                            @if($tingkatan == 'sd')
                            <option value="1" @if($kelas == 1) selected @endif class="kelasopt">1</option>
                            <option value="2" @if($kelas == 2) selected @endif class="kelasopt">2</option>
                            <option value="3" @if($kelas == 3) selected @endif class="kelasopt">3</option>
                            <option value="4" @if($kelas == 4) selected @endif class="kelasopt">4</option>
                            <option value="5" @if($kelas == 5) selected @endif class="kelasopt">5</option>
                            <option value="6" @if($kelas == 6) selected @endif class="kelasopt">6</option>
                            @elseif($tingkatan == 'smp')
                            <option value="4" @if($kelas == 7) selected @endif class="kelasopt">7</option>
                            <option value="5" @if($kelas == 8) selected @endif class="kelasopt">8</option>
                            <option value="6" @if($kelas == 9) selected @endif class="kelasopt">9</option>
                            @elseif($tingkatan == 'sma/smk')
                            <option value="4" @if($kelas == 10) selected @endif class="kelasopt">10</option>
                            <option value="5" @if($kelas == 11) selected @endif class="kelasopt">11</option>
                            <option value="6" @if($kelas == 12) selected @endif class="kelasopt">12</option>
                            @endif
                        </select>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-primary">
                            Cari    
                        </button>
                    </td>
                </tr>
            </table>
        </form>
    </li>
</ol>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Data Bank Soal
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>File Bank Soal</th>
                        <th>kelas & tingkatan</th>
                        <th>Semester</th>
                        <th>catatan</th>
                        <th>tgl upload</th>
                        <th>Nama Pengguna Pengupload</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($banksoal as $bs)
                    <tr>
                        <td><a href="/upload/soal/{{ $bs->id_b_soal }}.pdf" target="_blank">Link Soal</a></td>
                        <td>{{ $bs->kelas }} {{ $bs->tingkatan }}</td>
                        <td>
                            @if ($bs->semester == 1)
                                Ganjil
                            @else     
                                Genap                           
                            @endif
                        </td>
                        <td><a href="#" class="btn btn-primary cttn-detail" data-toggle="modal" data-target="#catatan-detail" data-tingkatan="{{ $bs->tingkatan }}" data-kelas="{{ $bs->kelas }}" data-semester="{{ $bs->semester }}" data-catatan="{{ $bs->catatan }}">Detail</a></td>
                        <td>{{ $bs->tgl_upload }}</td>
                        <td>{{ $bs->nama_lengkap }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $banksoal->links('pagination.default') }}</div>
        </div>
    </div>
</div> 

<div class="modal fade" id="catatan-detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Catatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span>Kelas</span><br>
                <input type="text" class="form-control" name="" id="kelas-detail" disabled><br>
                <span>Semester</span><br>
                <input type="text" class="form-control" name="" id="semester-detail" disabled><br>
                <span>Catatan</span><br>
                <textarea name="" id="catatan-detail-txt" class="form-control" cols="30" rows="10" disabled></textarea>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>




<script>
    $('.tingkatan').change(function(){
        var valtingkatan = $(this).val();
        $('.kelasopt').remove();
        if(valtingkatan == 'sd') {
            $('.kelas').append('<option value="1" class="kelasopt">1</option><option value="2" class="kelasopt">2</option><option value="3" class="kelasopt">3</option><option value="4" class="kelasopt">4</option><option value="5" class="kelasopt">5</option><option value="6" class="kelasopt">6</option>');
        } else if(valtingkatan == 'smp') {
            $('.kelas').append('<option value="7" class="kelasopt">7</option><option value="8" class="kelasopt">8</option><option value="9" class="kelasopt">9</option>');
        } else if(valtingkatan == 'sma/smk') {
            $('.kelas').append('<option value="10" class="kelasopt">10</option><option value="11" class="kelasopt">11</option><option value="12" class="kelasopt">12</option>');
        } else {
            $('.kelas').append('<option class="kelasopt">Tingkatan Harus Diiisi</option>')
        }

    });

    $('.cttn-detail').click(function() {
        var tingkatan = $(this).attr('data-tingkatan');
        var kelas     = $(this).attr('data-kelas');
        var semester  = $(this).attr('data-semester');
        var catatan   = $(this).attr('data-catatan');
        $('#kelas-detail').val(kelas+' '+tingkatan);
        $('#semester-detail').val(semester);
        $('#catatan-detail-txt').val(catatan);
    })
</script>
@endsection