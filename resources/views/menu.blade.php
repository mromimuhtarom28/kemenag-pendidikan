@php 
$idrole = Session::get('id_role');
$gurucount = App\Models\Guru::where('status', 1)
             ->count();

@endphp
<style>
.badge {
  border-radius: 50%;
  background-color: red;
  color: white;
}
</style>
{{-- <div class="sb-sidenav-menu">
    <div class="nav"> --}}
        @if($idrole == 1)
            @php 
                $idlogin = Session::get('idlogin');
                // $admvhr = App\Models\AdminVihara::join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                //         ->join('login', 'admin_vihara.id_login', 'login.id_login')
                //         ->where('admin_vihara.id_login', $idlogin)
                //         ->first();
            @endphp
                <a class="nav-link" href="{{ route('Dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <a class="nav-link" href="{{ route('PenerimaanGuru') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                    Menerima Akun Guru @if($gurucount != 0) <span class="badge">{{ $gurucount }}</span> @endif
                </a>
                <a class="nav-link" href="{{ route('Guru') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                    Guru 
                </a>
                <a class="nav-link" href="{{ route('BankSoal') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-file"></i></div>
                    Bank Soal
                </a>
                <a class="nav-link" href="{{ route('MateriDigital') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-digital-tachograph"></i></div>
                    Materi Digital
                </a>
                <a class="nav-link" href="{{ route('Sekolah') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-school"></i></div>
                    Sekolah
                </a>
        @elseif($idrole == 5)
        @php
            $usernamemenu = Session::get('username');
            $idloginmenu      = Session::get('idlogin');
            $guruformenu  = App\models\Guru::join('login', 'login.id_profil', 'guru.id_profil')
                            ->where('id_login', $idloginmenu)
                            ->first();
        @endphp
            @if($guruformenu->status == 2)
            <a class="nav-link" href="{{ route('Dashboard') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Dashboard
            </a>
            <a class="nav-link" href="{{ route('BankSoal') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-file"></i></div>
                Bank Soal {{ $usernamemenu }}
            </a>
            <a class="nav-link" href="{{ route('MateriDigital') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-digital-tachograph"></i></div>
                Materi Digital {{ $usernamemenu }}
            </a>
            <a class="nav-link" href="{{ route('BankSoal-Semua') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-file"></i></div>
                Bank Soal
            </a>
            <a class="nav-link" href="{{ route('MateriDigital-Semua') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-digital-tachograph"></i></div>
                Materi Digital
            </a>
            @endif
        @endif
        <a class="nav-link" href="{{ route('logout-process') }}">
            <div class="sb-nav-link-icon"><i class="fas fa-sign-out-alt"></i></div>
            Keluar
        </a>
    {{-- </div>
</div> --}}
