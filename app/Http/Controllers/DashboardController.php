<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class DashboardController extends Controller
{
    public function index()
    {
        $guru = DB::table('guru')->where('status', '!=', 3)->count();
        $guruditolak = DB::table('guru')->where('status', '=', 0)->count();
        $guruditerima = DB::table('guru')->where('status', '=', 2)->count();
        $banksoal     = DB::table('bank_soal')->count();
        $materidigital = DB::table('materi_digital')->count();
        $sekolah = DB::table('sekolah')->where('status', 1)->count();
        $role = Session::get('id_role');
        $username = Session::get('username');
        $idlogin = Session::get('idlogin');
        $banksoalself     = DB::table('bank_soal')->where('id_login', $idlogin)->count();
        $materidigitalself = DB::table('materi_digital')->where('id_login', $idlogin)->count();
        return view('pages.dashboard', compact('guru', 'guruditolak', 'guruditerima', 'banksoal', 'materidigital', 'sekolah', 'role', 'username', 'banksoalself', 'materidigitalself'));
    }
}
