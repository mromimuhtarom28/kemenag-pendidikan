<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use App\Models\MateriDigital;
use Validator;

class MateriDigitalController extends Controller
{
    public function index(Request $request)
    {
        $namainput = $request->namaInput;
        $tingkatan = $request->tingkatan;
        $kelas     = $request->kelas;
        $idrole    = Session::get('id_role');
        $idlogin   = Session::get('idlogin');
        
        if($idrole == 1):
            $materiDigitalMain = MateriDigital::join('login', 'materi_digital.id_login', '=', 'login.id_login')
                                ->join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil');
        else: 
            $materiDigitalMain = MateriDigital::join('login', 'materi_digital.id_login', '=', 'login.id_login')
                                ->join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil')
                                ->where('materi_digital.id_login', $idlogin);
        endif;
                
        if($namainput != NULL && $tingkatan != NULL && $kelas != NULL):
            $materiDigital = $materiDigitalMain->where('username', 'LIKE', '%'.$namainput.'%')
                             ->where('tingkatan', $tingkatan)
                             ->where('kelas', $kelas)
                             ->paginate(20);
        elseif($namainput != NULL && $tingkatan != NULL):
            $materiDigital = $materiDigitalMain->where('username', 'LIKE', '%'.$namainput.'%')
                             ->where('tingkatan', $tingkatan)
                             ->paginate(20);
        elseif($namainput != NULL && $kelas != NULL):
            $materiDigital = $materiDigitalMain->where('username', 'LIKE', '%'.$namainput.'%')
                             ->where('kelas', $kelas)
                             ->paginate(20);
        elseif($tingkatan != NULL && $kelas != NULL):
            $materiDigital = $materiDigitalMain->where('tingkatan', $tingkatan)
                             ->where('kelas', $kelas)
                             ->paginate(20);
        elseif($namainput != NULL):
            $materiDigital = $materiDigitalMain->where('username', 'LIKE', '%'.$namainput.'%')
                             ->paginate(20);
        elseif($tingkatan != NULL):
            $materiDigital = $materiDigitalMain->where('tingkatan', $tingkatan)
                             ->paginate(20);
        elseif($kelas != NULL):
            $materiDigital = $materiDigitalMain->where('kelas', $kelas)
                             ->paginate(20);
        else:
            $materiDigital = $materiDigitalMain->paginate(20);
        endif;
        $materiDigital->appends($request->all());

        $idrole   = Session::get('id_role');
        $idlogin  = Session::get('idlogin');
        return view('pages.materidigital', compact('materiDigital', 'idrole', 'idlogin', 'namainput', 'tingkatan', 'kelas'));
    }

    public function index_view(Request $request)
    {
        $namainput = $request->namaInput;
        $tingkatan = $request->tingkatan;
        $kelas     = $request->kelas;
        $idrole    = Session::get('id_role');
        $idlogin   = Session::get('idlogin');
        
        $materiDigitalMain = MateriDigital::join('login', 'materi_digital.id_login', '=', 'login.id_login')
                            ->join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil');
                
        if($namainput != NULL && $tingkatan != NULL && $kelas != NULL):
            $materiDigital = $materiDigitalMain->where('username', 'LIKE', '%'.$namainput.'%')
                             ->where('tingkatan', $tingkatan)
                             ->where('kelas', $kelas)
                             ->paginate(20);
        elseif($namainput != NULL && $tingkatan != NULL):
            $materiDigital = $materiDigitalMain->where('username', 'LIKE', '%'.$namainput.'%')
                             ->where('tingkatan', $tingkatan)
                             ->paginate(20);
        elseif($namainput != NULL && $kelas != NULL):
            $materiDigital = $materiDigitalMain->where('username', 'LIKE', '%'.$namainput.'%')
                             ->where('kelas', $kelas)
                             ->paginate(20);
        elseif($tingkatan != NULL && $kelas != NULL):
            $materiDigital = $materiDigitalMain->where('tingkatan', $tingkatan)
                             ->where('kelas', $kelas)
                             ->paginate(20);
        elseif($namainput != NULL):
            $materiDigital = $materiDigitalMain->where('username', 'LIKE', '%'.$namainput.'%')
                             ->paginate(20);
        elseif($tingkatan != NULL):
            $materiDigital = $materiDigitalMain->where('tingkatan', $tingkatan)
                             ->paginate(20);
        elseif($kelas != NULL):
            $materiDigital = $materiDigitalMain->where('kelas', $kelas)
                             ->paginate(20);
        else:
            $materiDigital = $materiDigitalMain->paginate(20);
        endif;
        $materiDigital->appends($request->all());

        $idrole   = Session::get('id_role');
        $idlogin  = Session::get('idlogin');
        return view('pages.materidigital-semua-guru', compact('materiDigital', 'idrole', 'idlogin', 'namainput', 'tingkatan', 'kelas'));
    }

    public function uploadmateri(Request $request)
    {
        $file      = $request->file('uploadMateri');
        $kelas     = $request->kelas;
        $semester  = $request->semester;
        $tingkatan = $request->tingkatan;
        $idlogin    = Session::get('idlogin');

        $validator = Validator::make($request->all(), [
            'uploadMateri'  => 'required',
            'kelas'         => 'required',
            'semester'      => 'required',
            'tingkatan'     => 'required',
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;
        $semesterarray = array(1,2);
        if(!in_array($semester, $semesterarray)): 
            alert()->error('Mohon maaf semester wajib diisi dengan pilihan 1 atau 2');
            return back();
        endif;
        $tingakatanarray = array('sd', 'smp', 'sma/smk');
        if(!in_array($tingkatan, $tingakatanarray)):
            alert()->error('Mohon maaf wajib memilih tingkatan sd, smp atau sma/smk');
            return back(); 
        endif;

        $kelasarray = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        if(!in_array($kelas, $kelasarray)):
            alert()->error('Mohon maaf kelas wajib memilih kelas 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 dan 12');
            return back(); 
        endif;

        $materidigital = MateriDigital::orderby('id_materi_digital', 'desc')->first();

        if($materidigital):
            $idmateridigital =  idgenerate($materidigital->id_materi_digital, 'MD');
        else:
            $idmateridigital = idgenerate('', 'MD');
        endif;
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        $file->move(public_path().'/upload/materi_digital/', $idmateridigital.'.'.$extension);
        MateriDigital::create([
            'id_materi_digital' => $idmateridigital,
            'id_login'          => $idlogin,
            'nama_file'         => $idmateridigital.'.'.$extension,
            'tingkatan'         => $tingkatan,
            'semester'          => $semester,
            'kelas'             => $kelas,
            'tgl_upload'        => Carbon::now('GMT+7')
        ]);


        alert()->success('Input data berhasil');
        return back();

    }

    public function updatedata(Request $request)
    {
        $pk        = $request->pk;
        $file  = $request->file('uploadMateri');
        $kelas     = $request->kelas;
        $tingkatan = $request->tingkatan;
        $semester  = $request->semester;
        $idlogin   = Session::get('idlogin');

        $validator = Validator::make($request->all(), [
            'kelas'         => 'required',
            'semester'      => 'required',
            'tingkatan'     => 'required',
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;
        $semesterarray = array(1,2);
        if(!in_array($semester, $semesterarray)): 
            alert()->error('Mohon maaf semester wajib diisi dengan pilihan 1 atau 2');
            return back();
        endif;
        $tingakatanarray = array('sd', 'smp', 'sma/smk');
        if(!in_array($tingkatan, $tingakatanarray)):
            alert()->error('Mohon maaf wajib memilih tingkatan sd, smp atau sma/smk');
            return back(); 
        endif;

        $kelasarray = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        if(!in_array($kelas, $kelasarray)):
            alert()->error('Mohon maaf kelas wajib memilih kelas 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 dan 12');
            return back(); 
        endif;

        $materidigital = MateriDigital::where('id_materi_digital', $pk)->first();

        if(!$materidigital):
            alert()->error('Mohon maaf id materi digital tidak ada silahkan coba lagi');
            return back();
        endif;
        if($file != NULL):
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $file->move(public_path().'/upload/materi_digital/', $pk.'.'.$extension);
            MateriDigital::where('id_materi_digital', $pk)->update([
                'id_login'          => $idlogin,
                'nama_file'         => $pk.'.'.$extension,
                'tingkatan'         => $tingkatan,
                'semester'          => $semester,
                'kelas'             => $kelas,
                'tgl_upload'        => Carbon::now('GMT+7')
            ]);
        else: 
            MateriDigital::where('id_materi_digital', $pk)->update([
                'id_login'          => $idlogin,
                'tingkatan'         => $tingkatan,
                'semester'          => $semester,
                'kelas'             => $kelas,
                'tgl_upload'        => Carbon::now('GMT+7')
            ]);
        endif;


        alert()->success('Input data berhasil');
        return back();
    }

    public function destroy(Request $request)
    {
        $pk = $request->pk;
        $materidigital = MateriDigital::where('id_materi_digital', $pk)->first();

        if(!$materidigital):
            alert()->error('Mohon maaf id materi digital tidak ada silahkan coba lagi');
            return back(); 
        endif;
        $soal = public_path().'/upload/materi_digital/'.$materidigital->nama_file;
        File::delete($soal);
        MateriDigital::where('id_materi_digital', $pk)->delete();

        alert()->success('Hapus data telah berhasil');
        return back();

    }
}
