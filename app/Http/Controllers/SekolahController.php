<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sekolah;
use Session;
use Validator;

class SekolahController extends Controller
{
    public function index(Request $request)
    {
        $namasekolah = $request->namaSekolah;
        $idrole = Session::get('id_role');
        if($idrole != 1): 
            alert()->error('Mohon maaf anda tidak memiliki akses ke halaman ini');
            return back();
        endif;

        if($namasekolah != NULL):
            $sekolah = Sekolah::where('nama_sekolah', 'LIKE', '%'.$namasekolah.'%')
                       ->paginate(20);
        else: 
            $sekolah = Sekolah::paginate(20);
        endif;

        $sekolah->appends($request->all());

        return view('pages.sekolah', compact('sekolah', 'namasekolah'));
    }

    public function updateImage(Request $request)
    {
        $pk = $request->pk;
        $img = $request->file('file-sekolah');
        $idrole = Session::get('id_role');
        if($idrole != 1): 
            alert()->error('Mohon maaf anda tidak memiliki akses ke halaman ini');
            return back();
        endif;

        $validator = Validator::make($request->all(), [
            'pk'          => 'required|max:25',
            'file-sekolah' => 'required|mimes:jpg'
        ]);

        if($validator->fails()):
            alert()->error($validator->error()->all());
            return back(); 
        endif;

        $img->move(public_path().'/upload/sekolah/', $pk.'.jpg');

        alert()->success('Ubah foto telah berhasil');
        return back();
    }

    public function insert(Request $request)
    {
        $img           = $request->file('file');
        $nmsekolah      = $request->namaSekolah;
        $alamat        = $request->alamat;
        $status        = $request->status;
        $kodePos       = $request->$kodePos;
        $noijinsekolah = $request->noIjinSekolah;
        $kecamatan     = $request->kecamatan;
        $idrole = Session::get('id_role');
        if($idrole != 1): 
            alert()->error('Mohon maaf anda tidak memiliki akses ke halaman ini');
            return back();
        endif;

        $validator = Validator::make($request->all(), [
            'file'          => 'required|mimes:jpg',
            'namaSekolah'   => 'required|max:255',
            'kodePos'       => 'required|max:5',
            'noIjinSekolah' => 'required|max:25',
            'kecamatan'     => 'required|max:255',
            'alamat'        => 'required',
            'status'        => 'required|numeric'
        ]);

        if($validator->fails()):
            alert()->error($validator->error()->all());
            return back(); 
        endif;
        if(!is_numeric($kodePos)):
            alert()->error('Mohon maaf kode pos harus dalam bentuk angka');
            return back(); 
        endif;

        $arraystatus = array(0,1);
        if(!in_array($status, $arraystatus)):
            return 'status harus pilih aktif atau tidak aktif';
        endif;

        try {
            $sekolah = Sekolah::orderby('id_sekolah', 'desc')->first();

            if($sekolah):
                $idsekolah =  idgenerate($sekolah->id_sekolah, 'SK');
            else:
                $idsekolah = idgenerate('', 'SK');
            endif;
            $img->move(public_path().'/upload/sekolah/', $idsekolah.'.jpg');

            Sekolah::create([
                'id_sekolah'      => $idsekolah,
                'nama_sekolah'    => $nmsekolah,
                'no_ijin_sekolah' => $noijinsekolah,
                'kose_pos'        => $kodePos,
                'kecamatan'       => $kecamatan,
                'alamat'          => $alamat,
                'status'          => $status
            ]);

            alert()->success('Input data telah berhasil');
            return back();

        } catch(Exception $e) {
            alert()->error($e->getMessage());
            return back();
        }
    }

    public function update(Request $request)
    {
        $pk    = $request->pk;
        $name  = $request->name;
        $value = $request->value;
        $idrole = Session::get('id_role');
        if($idrole != 1): 
            alert()->error('Mohon maaf anda tidak memiliki akses ke halaman ini');
            return back();
        endif;

        Sekolah::where('id_sekolah', $pk)->update([
            $name => $value
        ]);
    }
}
