<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Guru;
use Validator;
use Session;

class PenerimaanGuruController extends Controller
{
    public function index()
    {
        $idrole = Session::get('id_role');
        if($idrole != 1): 
            alert()->error('Mohon maaf anda tidak memiliki akses ke halaman ini');
            return back();
        endif;
        $guru = Guru::where('status', 1)
                ->join('profil_login', 'profil_login.id_profil', '=', 'guru.id_profil')
                ->join('login', 'login.id_profil', '=', 'profil_login.id_profil')
                ->paginate(20);
        return view('pages.penerimaanguru', compact('guru'));
    }

    public function approvedecline(Request $request)
    {
        $idrole = Session::get('id_role');
        if($idrole != 1): 
            alert()->error('Mohon maaf anda tidak memiliki akses ke halaman ini');
            return back();
        endif;
        $pk      = $request->pk;
        $status  = $request->status;
        $catatan = $request->catatan;
        if($status == 0): 
            $validator = Validator::make($request->all(), [
                'catatan' => 'required'
            ]);

            if($validator->fails()):
                alert()->error($validator->errors()->all());
                return back(); 
            endif;

        endif;

        if($status == 2): 
            Guru::where('id_guru', $pk)->update([
                'status'    =>  $status
            ]);
            alert()->success('Menerima akun guru');
        elseif($status == 0): 
            Guru::where('id_guru', $pk)->update([
                'status'    =>  $status,
                'catatan'   =>  $catatan
            ]);
            alert()->success('Menolak akun guru');
        endif;
        return back();
    }
}