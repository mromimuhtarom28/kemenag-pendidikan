<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Guru;
use App\Models\Sekolah;
use Exception;
use Validator;
use Illuminate\Support\Facades\Http;

class ProfileController extends Controller
{
    public function index()
    {
        $idlogin = Session::get('idlogin');
        $guru    = Guru::join('profil_login', 'profil_login.id_profil', '=', 'guru.id_profil')
                   ->join('login', 'login.id_profil', '=', 'guru.id_profil')
                   ->where('login.id_login', $idlogin)
                   ->first();
        
        $sekolah = Sekolah::where('status', 1)->get();

        return view('pages.profile', compact('guru', 'sekolah'));
    }

    public function update(Request $request)
    {
        $img         = $request->file('gambarProfile');
        $ktp         = $request->noKtp;
        $nuptk       = $request->nuptk;
        $namalengkap = $request->namaLengkap;
        $sekolah     = $request->sekolah;
        $telp        = $request->noTelp;
        $tgllhr      = $request->tanggalLahir;
        $alamat      = $request->alamat;

        $validator = Validator::make($request->all(), [
            'gambarProfile' => 'required|mimes:jpg',
            'noKtp'         => 'required|max:16',
            'nuptk'         => 'required|max:16',
            'namaLengkap'   => 'required|max:255',
            'sekolah'       => 'required',
            'noTelp'        => 'required|max:15',
            'tanggalLahir'  => 'required',
            'alamat'        => 'required'
        ]);

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back();
        endif;
    }
}
