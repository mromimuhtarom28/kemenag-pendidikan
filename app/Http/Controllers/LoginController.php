<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\Guru;
use Validator;
use DB;
use Cache;
use App\Models\User;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function Login(Request $request)
    {
        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){

            $listrole = array(1,5);
            if(!in_array(Auth::user()->id_role, $listrole)):
                alert()->error('Mohon maaf anda tidak memiliki akses untuk website ini');
                return back();
            endif;
            if(Auth::user()->id_role == 5):
                $AdminGuru = Guru::join('profil_login', 'profil_login.id_profil', '=', 'guru.id_profil')
                                ->join('login', 'profil_login.id_profil', 'login.id_profil')
                                ->where('login.id_login', Auth::user()->id_login)
                                ->first();
                if($AdminGuru->status == 3):
                    alert()->error('Mohon maaf nama pengguna atau kata sandi anda salah silahkan coba lagi !!!');
                    return back();
                endif;
            endif;
            $array = array(2,3,4);
            if(in_array(Auth::user()->id_role, $array)):
                alert()->error('Mohon maaf anda tidak memiliki akses kesini');
                return back(); 
            endif;
            Session::put('idlogin', Auth::user()->id_login);
            Session::put('username', Auth::user()->username);
            Session::put('id_role',Auth::user()->id_role);
            Session::put('login',TRUE);
            $username        = $request->username;
            $password        = $request->password;

            if(Auth::user()->id_role == 5):
                $AdminGuru = Guru::join('profil_login', 'profil_login.id_profil', '=', 'guru.id_profil')
                                ->join('login', 'profil_login.id_profil', 'login.id_profil')
                                ->where('login.id_login', Auth::user()->id_login)
                                ->first();
                if($AdminGuru->status == 0): 
                    return redirect(route('Profile'));
                elseif($AdminGuru->status == 1):
                    Session::put('login',FALSE);
                    Session::flush();
                    Cache::flush();
                    alert()->error('Menunggu diterima dari admin kementrian agama');
                    return back();
                else: 
                    return redirect(route('Dashboard'));
                endif;
            else: 
                return redirect(route('Dashboard'));
            endif;
  
         } else {
            $username = $request->username;
            alert()->error('ErrorAlert', 'Mohon maaf nama pengguna atau kata sandi anda salah silahkan coba lagi !!!');
            return back();
        }
    }

    public function LogOut()
    {
        Session::flush();
        Cache::flush();

        alert()->success('Terima kasih');
        return redirect()->route('Login');
    }
}