<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BankSoal;
use Session;
use Validator;
use Carbon\Carbon;
use File;

class BankSoalController extends Controller
{
    public function index(Request $request)
    {   
        $namainput = $request->namaInput;
        $tingkatan = $request->tingkatan;
        $kelas     = $request->kelas;
        $idrole    = Session::get('id_role');
        $idlogin   = Session::get('idlogin');
        
        if($idrole == 1):
            $banksoalMain = BankSoal::join('login', 'bank_soal.id_login', '=', 'login.id_login')
                            ->join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil');
        else: 
            $banksoalMain = BankSoal::join('login', 'bank_soal.id_login', '=', 'login.id_login')
                            ->join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil')
                            ->where('bank_soal.id_login', $idlogin);
        endif;
                
        if($namainput != NULL && $tingkatan != NULL && $kelas != NULL):
            $banksoal = $banksoalMain->where('username', 'LIKE', '%'.$namainput.'%')
                        ->where('tingkatan', $tingkatan)
                        ->where('kelas', $kelas)
                        ->paginate(20);
        elseif($namainput != NULL && $tingkatan != NULL):
            $banksoal = $banksoalMain->where('username', 'LIKE', '%'.$namainput.'%')
                        ->where('tingkatan', $tingkatan)
                        ->paginate(20);
        elseif($namainput != NULL && $kelas != NULL):
            $banksoal = $banksoalMain->where('username', 'LIKE', '%'.$namainput.'%')
                        ->where('kelas', $kelas)
                        ->paginate(20);
        elseif($tingkatan != NULL && $kelas != NULL):
            $banksoal = $banksoalMain->where('tingkatan', $tingkatan)
                        ->where('kelas', $kelas)
                        ->paginate(20);
        elseif($namainput != NULL):
            $banksoal = $banksoalMain->where('username', 'LIKE', '%'.$namainput.'%')
                        ->paginate(20);
        elseif($tingkatan != NULL):
            $banksoal = $banksoalMain->where('tingkatan', $tingkatan)
                        ->paginate(20);
        elseif($kelas != NULL):
            $banksoal = $banksoalMain->where('kelas', $kelas)
                        ->paginate(20);
        else:
            $banksoal = $banksoalMain->paginate(20);
        endif;
        $banksoal->appends($request->all());

        $idrole   = Session::get('id_role');
        $idlogin  = Session::get('idlogin');
        return view('pages.banksoal', compact('banksoal', 'idrole', 'idlogin', 'namainput', 'tingkatan', 'kelas'));
    }

    public function index_view(Request $request)
    {   
        $namainput = $request->namaInput;
        $tingkatan = $request->tingkatan;
        $kelas     = $request->kelas;
        
        $banksoalMain = BankSoal::join('login', 'bank_soal.id_login', '=', 'login.id_login')
                        ->join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil');
                
        if($namainput != NULL && $tingkatan != NULL && $kelas != NULL):
            $banksoal = $banksoalMain->where('username', 'LIKE', '%'.$namainput.'%')
                        ->where('tingkatan', $tingkatan)
                        ->where('kelas', $kelas)
                        ->paginate(20);
        elseif($namainput != NULL && $tingkatan != NULL):
            $banksoal = $banksoalMain->where('username', 'LIKE', '%'.$namainput.'%')
                        ->where('tingkatan', $tingkatan)
                        ->paginate(20);
        elseif($namainput != NULL && $kelas != NULL):
            $banksoal = $banksoalMain->where('username', 'LIKE', '%'.$namainput.'%')
                        ->where('kelas', $kelas)
                        ->paginate(20);
        elseif($tingkatan != NULL && $kelas != NULL):
            $banksoal = $banksoalMain->where('tingkatan', $tingkatan)
                        ->where('kelas', $kelas)
                        ->paginate(20);
        elseif($namainput != NULL):
            $banksoal = $banksoalMain->where('username', 'LIKE', '%'.$namainput.'%')
                        ->paginate(20);
        elseif($tingkatan != NULL):
            $banksoal = $banksoalMain->where('tingkatan', $tingkatan)
                        ->paginate(20);
        elseif($kelas != NULL):
            $banksoal = $banksoalMain->where('kelas', $kelas)
                        ->paginate(20);
        else:
            $banksoal = $banksoalMain->paginate(20);
        endif;
        $banksoal->appends($request->all());

        $idrole   = Session::get('id_role');
        $idlogin  = Session::get('idlogin');
        return view('pages.banksoal-semua-guru', compact('banksoal', 'idrole', 'idlogin', 'namainput', 'tingkatan', 'kelas'));
    }

    public function inputsoal(Request $request)
    {
        $file      = $request->file('soal');
        $tingkatan = $request->tingkatan;
        $kelas     = $request->kelas;
        $catatan   = $request->catatan;
        $semester  = $request->semester;
        $idlogin   = Session::get('idlogin');
        $idrole    = Session::get('id_role');
        $datenow   = Carbon::now('GMT+7');

        $validator = Validator::make($request->all(), [
            'soal'      => 'required|mimes:pdf',
            'tingkatan' => 'required',
            'kelas'     => 'required',
            'semester'  => 'required',
            'catatan'   => 'required'
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;

        $banksoal = BankSoal::orderby('id_b_soal', 'desc')->first();

        if($banksoal):
            $idbanksoal =  idgenerate($banksoal->id_b_soal, 'BS');
        else:
            $idbanksoal = idgenerate('', 'BS');
        endif;

        $file->move(public_path().'/upload/soal/', $idbanksoal.'.pdf');
        BankSoal::create([
            'id_b_soal'  => $idbanksoal,
            'id_login'   => $idlogin,
            'kelas'      => $kelas,
            'semester'   => $semester,
            'tingkatan'  => $tingkatan,
            'catatan'    => $catatan,
            'tgl_upload' => $datenow
        ]);        

        alert()->success('input telah berhasil');
        return back();
    }

    public function edit(Request $request) 
    {
        $file      = $request->file('soal');
        $tingkatan = $request->tingkatan;
        $kelas     = $request->kelas;
        $catatan   = $request->catatan;
        $semester  = $request->semester;
        $pk        = $request->pk;
        $datenow   = Carbon::now('GMT+7');

        $banksoal = BankSoal::where('id_b_soal', $pk)->first();

        if(!$banksoal):
            alert()->error('Mohon maaf data id bank soal tidak ditemukan'); 
            return back();
        endif;

        if($file):
            $validator = Validator::make($request->all(), [
                'pk'        => 'required',
                'soal'      => 'required|mimes:pdf',
                'tingkatan' => 'required',
                'kelas'     => 'required',
                'semester'  => 'required',
                'catatan'   => 'required'
            ]);
    
            if($validator->fails()): 
                alert()->error($validator->errors()->all());
                return back();
            endif;

            $file->move(public_path().'/upload/soal/', $pk.'.pdf');
        else: 
            $validator = Validator::make($request->all(), [
                'pk'        => 'required',
                'tingkatan' => 'required',
                'kelas'     => 'required',
                'semester'  => 'required',
                'catatan'   => 'required'
            ]);
    
            if($validator->fails()): 
                alert()->error($validator->errors()->all());
                return back();
            endif;
        endif;

        BankSoal::where('id_b_soal', $pk)->update([
            'kelas'      => $kelas,
            'semester'   => $semester,
            'tingkatan'  => $tingkatan,
            'catatan'    => $catatan,
            'tgl_upload' => $datenow
        ]);        

        alert()->success('ubah data telah berhasil');
        return back();
    }

    public function destroy(Request $request)
    {
        $pk = $request->pk;
        $banksoal = BankSoal::where('id_b_soal', $pk)->first();

        if(!$banksoal):
            alert()->error('Mohon maaf id bank soal tidak ada silahkan coba lagi');
            return back(); 
        endif;
        $soal = public_path().'/upload/soal/'.$pk.'.pdf';
        File::delete($soal);
        BankSoal::where('id_b_soal', $pk)->delete();

        alert()->success('Hapus data telah berhasil');
        return back();

    }
}
