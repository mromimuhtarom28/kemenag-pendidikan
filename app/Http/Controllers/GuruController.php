<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Guru;
use Session;
use Validator;
use App\Models\User;

class GuruController extends Controller
{
    public function index()
    {
        $idrole = Session::get('id_role');
        if($idrole != 1): 
            alert()->error('Mohon maaf anda tidak memiliki akses ke halaman ini');
            return back();
        endif;
        $guru = Guru::whereIn('status', [0, 2])
                ->join('profil_login', 'profil_login.id_profil', '=', 'guru.id_profil')
                ->join('login', 'login.id_profil', '=', 'profil_login.id_profil')
                ->paginate(20);
        return view('pages.guru', compact('guru'));
    }

    public function changepwd(Request $request)
    {
        $idrole = Session::get('id_role');
        if($idrole != 1): 
            alert()->error('Mohon maaf anda tidak memiliki akses ke halaman ini');
            return back();
        endif;
        $pk       = $request->pk;
        $newpwd   = $request->kataSandiBaru;
        $pwdlogin = $request->kataSandiSedangLogin;
        $idlogin  = Session::get('idlogin');
        $user     = User::where('id_login', $idlogin)->first();
        $validator = Validator::make($request->all(), [
            'kataSandiBaru'        => 'required|max:255',
            'kataSandiSedangLogin' => 'required',
            'pk'                   => 'required'
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;

        if(password_verify($pwdlogin, $user->password)):
            $bcrypt = bcrypt($newpwd);
            User::where('id_login', $idlogin)->update([
                'password' => $bcrypt
            ]);

            alert()->success('Ubah kata sandi telah berhasil');
            return back();
        else: 
            alert()->error('Kata sandi yang sedang login salah dan tidak sesuai silahkan coba kemabli');
            return back();
        endif;
    }

    public function destroydata(Request $request)
    {
        $idrole = Session::get('id_role');
        if($idrole != 1): 
            alert()->error('Mohon maaf anda tidak memiliki akses ke halaman ini');
            return back();
        endif;
        $pk       = $request->pk;
        $pwdlogin = $request->kataSandiSedangLogin;
        $idlogin  = Session::get('idlogin');
        $validator = Validator::make($request->all(), [
            'kataSandiSedangLogin' => 'required',
            'pk'                   => 'required'
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;

        // $admin = Guru::where('id_login', $pk)->first();
        $guru  = Guru::join('login', 'login.id_profil', '=', 'guru.id_profil')
                 ->where('id_login', $pk)
                 ->first();

        $user  = User::where('id_login', $idlogin)->first();
        if(password_verify($pwdlogin, $user->password)):
            Guru::where('id_guru', $guru->id_guru)->update([
                'status' => 3
            ]);

            alert()->success('Hapus akun telah berhasil');
            return back();
        else: 
            alert()->error('Kata sandi yang sedang login salah dan tidak sesuai silahkan coba kemabli');
            return back();
        endif;

    }
}
