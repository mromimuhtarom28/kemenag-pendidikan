<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use App\Models\AdminVihara;

class AuthLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $login   = Session::get('login');
        $idlogin = Session::get('idlogin');
        $role    = Session::get('id_role');

        if($login == TRUE): 
            return $next($request);
        else:
            alert()->error('ErrorAlert', 'Mohon maaf session sudah habis');
            return redirect()->route('logout-process');
        endif;
    }
}
