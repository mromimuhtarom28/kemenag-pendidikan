<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Guru;
use Session;


class ActivationAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $login   = Session::get('login');
        $idlogin = Session::get('idlogin');
        $role    = Session::get('id_role');
        if($role == 5): 
            $AdminGuru = Guru::join('profil_login', 'profil_login.id_profil', '=', 'guru.id_profil')
                            ->join('login', 'profil_login.id_profil', 'login.id_profil')
                            ->where('login.id_login', $idlogin)
                            ->first();
            if($AdminGuru->status == 0): 
                return redirect()->route('Profile');
            elseif($AdminGuru->status == 1):
                alert()->error('Menunggu diterima dari admin kementrian agama');
                return back();
            else: 
                return $next($request);
            endif;
        else:
            return $next($request);
        endif;
    }
}
