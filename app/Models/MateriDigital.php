<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MateriDigital extends Model
{
    use HasFactory;
    protected $table = 'materi_digital';
    protected $guarded = [];
    protected $primaryKey = 'id_materi_digital';
    public $timestamps = false;
    public $incrementing = false;
}
