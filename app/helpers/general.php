<?php 
use Carbon\Carbon;

function LastId($idLastFDatabase)
{
    $lastid     = substr($idLastFDatabase, -3);
    $lastiddate = substr($idLastFDatabase, -5, 2);
    $idplus     = $lastid + 001;

    return ['last_id'=> $lastid, 'last_id_date' => $lastiddate, 'id_plus' => $idplus];
}

function does_url_exists($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($code == 200) {
        $status = true;
    } else {
        $status = false;
    }
    curl_close($ch);
    return $status;
}

function idgenerate($data, $firstid){
    $idfirst    = 1;
    $createdate = Carbon::now('GMT+7');
    $year       = Carbon::now('GMT+7')->format('y');
    $month      = Carbon::now('GMT+7')->format('m');
    $date       = Carbon::now('GMT+7')->format('d');
    if ($data) {
        $lastId     = LastId($data);
        $idfirst    = 1;
        if ($date === $lastId['last_id_date']) {
            if ($lastId['last_id'] >= '009' && $lastId['last_id'] <= '099') {
                $idincrement = $firstid . $year . $month . $date . '0' . $lastId['id_plus'];
            } else if ($lastId['last_id'] >= '099' && $lastId['last_id'] <= '999') {
                $idincrement = $firstid . $year . $month . $date  . $lastId['id_plus'];
            } else {
                $idincrement = $firstid . $year . $month . $date . '00' . $lastId['id_plus'];
            }
        } else if ($date !== $lastId['last_id_date']) {
            $idincrement = $firstid . $year . $month . $date . '00' . $idfirst;
        }
    } else {
        $idincrement = $firstid . $year . $month . $date . '001';
    }

    return $idincrement;
}

function str_endis($val)
{
    if($val == 0): 
        $name = 'Tidak Aktif';
    elseif($val == 1):
        $name = 'Aktif';
    else: 
        $name = 'Null';
    endif;

    return $name;
}
?>