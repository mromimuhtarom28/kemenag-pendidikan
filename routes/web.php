<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'LoginController@index')->name('Login');
Route::group(['prefix' => 'login'], function() {
    Route::post('/login-process', 'LoginController@Login')->name('Login-Process');
    Route::get('/logout-process', 'LoginController@LogOut')->name('logout-process'); 
});

Route::middleware('authlogin')->group(function(){
    Route::group(['prefix' => 'Profile'], function(){
        Route::get('/Profil-view', 'ProfileController@index')->name('Profile');
        Route::post('/Profile-update', 'ProfilController@update')->name('Profile-Update');
    });
    Route::middleware('ActivationAccount')->group(function(){
        Route::group(['prefix' => 'Dashboard'], function(){
            Route::get('Dashboard-view', 'DashboardController@index')->name('Dashboard');
        });

        Route::group(['prefix' => 'approveguru'], function(){
            Route::get('PenerimaanGuru-view', 'PenerimaanGuruController@index')->name('PenerimaanGuru');
            Route::post('PenerimaanGuru-approvedec', 'PenerimaanGuruController@approvedecline')->name('PenerimaanGuru-appdec');
        });

        Route::group(['prefix' => 'guru'], function(){
            Route::get('Guru-view', 'GuruController@index')->name('Guru');
            Route::post('Guru-chgpwd', 'GuruController@changepwd')->name('Guru-chgpwd');
            Route::post('Guru-delete', 'GuruController@destroydata')->name('Guru-destroy');
        });

        Route::group(['prefix' => 'BankSoal'], function(){
            Route::get('BankSoal-View', 'BankSoalController@index')->name('BankSoal');
            Route::post('BankSoal-Insert', 'BankSoalController@inputsoal')->name('BankSoal-Input');
            Route::post('BankSoal-Update', 'BankSoalController@edit')->name('BankSoal-Edit');
            Route::post('BankSoal-Delete', 'BankSoalController@destroy')->name('BankSoal-Delete');
        });
        Route::group(['prefix' => 'BankSoal-Semua'], function() {
            Route::get('BankSoal-semua-View', 'BankSoalController@index_view')->name('BankSoal-Semua');
        });

        Route::group(['prefix' => 'MateriDigital'], function() {
            Route::get('MateriDigital-View', 'MateriDigitalController@index')->name('MateriDigital');
            Route::post('MateriDigital-Insert', 'MateriDigitalController@uploadmateri')->name('MateriDigital-Input');
            Route::post('MateriDigital-Update', 'MateriDIgitalController@updatedata')->name('MateriDigital-update');
            Route::post('MateriDigital-Delete', 'MateriDIgitalController@destroy')->name('MateriDigital-Delete');
        });

        Route::group(['prefix' => 'MateriDigital-Semua'], function() {
            Route::get('MateriDigitial-semua-View', 'MateriDigitalController@index_view')->name('MateriDigital-Semua');
        });
        Route::group(['prefix' => 'Sekolah'], function() {
            Route::get('sekolah-view', 'SekolahController@index')->name('Sekolah');
            Route::post('sekolah-imgchange', 'SekolahController@updateImage')->name('Sekolah-ImgChange');
            Route::post('sekolah-process-input', 'SekolahController@insert')->name('Sekolah-processInsert');
            Route::post('sekolah-update', 'SekolahController@update')->name('Sekolah-update');
        });
    });
});
